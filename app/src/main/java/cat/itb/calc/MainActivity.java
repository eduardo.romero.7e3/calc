package cat.itb.calc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.InputMismatchException;

public class MainActivity extends AppCompatActivity {

    Button zero;
    Button one;
    Button two;
    Button three;
    Button four;
    Button five;
    Button six;
    Button seven;
    Button eight;
    Button nine;
    Button dot;
    Button sum;
    Button subs;
    Button mult;
    Button slash;
    Button eq;
    Button del;
    TextView result;
    int operation;
    boolean sign = false;
    boolean showResult = false;
    double a = 0;
    double b = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        zero = findViewById(R.id.zero);
        one = findViewById(R.id.one);
        two = findViewById(R.id.two);
        three = findViewById(R.id.three);
        four = findViewById(R.id.four);
        five = findViewById(R.id.five);
        six = findViewById(R.id.six);
        seven = findViewById(R.id.seven);
        eight = findViewById(R.id.eight);
        nine = findViewById(R.id.nine);
        dot = findViewById(R.id.dot);
        sum = findViewById(R.id.sum);
        subs = findViewById(R.id.subs);
        mult = findViewById(R.id.mult);
        slash = findViewById(R.id.slash);
        eq = findViewById(R.id.eq);
        del = findViewById(R.id.del);
        result = findViewById(R.id.result);

        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeNum(zero);
            }
        });

        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeNum(one);
            }
        });

        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeNum(two);
            }
        });

        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeNum(three);
            }
        });

        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeNum(four);
            }
        });

        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeNum(five);
            }
        });

        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeNum(six);
            }
        });

        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeNum(seven);
            }
        });

        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeNum(eight);
            }
        });

        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                writeNum(nine);
            }
        });

        dot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result.append(".");
            }
        });

        sum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!sign) a = Double.parseDouble(result.getText().toString());
                else b = Double.parseDouble(result.getText().toString());
                operation = 0;
                result.setText("+");
                sign = true;
            }
        });

        subs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!sign) a = Double.parseDouble(result.getText().toString());
                else b = Double.parseDouble(result.getText().toString());
                operation = 1;
                result.setText("-");
                sign = true;
            }
        });

        mult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!sign) a = Double.parseDouble(result.getText().toString());
                else b = Double.parseDouble(result.getText().toString());
                operation = 2;
                result.setText("x");
                sign = true;
            }
        });

        slash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!sign) a = Double.parseDouble(result.getText().toString());
                else b = Double.parseDouble(result.getText().toString());
                operation = 3;
                result.setText("/");
                sign = true;
            }
        });

        eq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!showResult){
                    b = Double.parseDouble(result.getText().toString());
                    ongoingResult(operation);
                    result.setText(String.valueOf(a));
                    showResult = true;
                }
            }
        });

        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result.setText("");
                sign = false;
                a = 0;
                b = 0;
            }
        });
    }
    public void writeNum(Button b){
        if(showResult){
            result.setText("");
            showResult = false;
        }
        if(sign){
            result.setText("");
            sign = false;
        }
        result.append(b.getText().toString());
    }

    public void ongoingResult(int op){
        switch(op){
            case 0:
                a = a + b;
                break;
            case 1:
                a = a - b;
                break;
            case 2:
                a = a * b;
                break;
            case 3:
                a = a / b;
                break;
        }
    }
}